	<!DOCTYPE html>
	<html>
	<head>
		<title></title>
	</head>
	<body>

		@yield('cuerpo')
		
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<h3>Registrate</h3>
		@if (count($errors)>0)
		<div class="alert alert-danger">
			<ul>
			@foreach ($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
			</ul>
		</div>
		@endif

		{!!Form::open(array('url'=>'expectativa','method'=>'POST','autocomplete'=>'off'))!!}
		{{Form::token()}}
		<div class="form-group">
			<label for="nombre">Nombre</label>
			<input type="text" name="nombre" class="form-control" placeholder="Nombre...">	
		</div>
		<div class="form-group">
			<label for="correo">Correo</label>
			<input type="text" name="correo" class="form-control" placeholder="Correo...">	
		</div>
		
		<div class="form-group">
			<button class="btn btn-primary" type="submit">Guardar</button>
			<button class="btn btn-danger"  type="" >Cancelar</button>
		</div>
		
		{!!Form::close()!!}
			<form method="post" action="https://gateway.payulatam.com/ppp-web-gateway/pb.zul" accept-charset="UTF-8">
  			<input type="image" border="0" alt="" src="http://www.payulatam.com/img-secure-2015/boton_pagar_pequeno.png" onClick="this.form.urlOrigen.value = window.location.href;"/>
  			<input name="buttonId" type="hidden" value="QPbfhXF3NwrARKHT+P1XDCCARfeMe/ae41+5HELoBx0n8cxayUvs4w=="/>
  			<input name="merchantId" type="hidden" value="632420"/>
  			<input name="accountId" type="hidden" value="634765"/>
  			<input name="description" type="hidden" value="venta creinmus"/>
  			<input name="referenceCode" type="hidden" value="01"/>
  			<input name="amount" type="hidden" value="20000"/>
  			<input name="tax" type="hidden" value="0"/>
  			<input name="taxReturnBase" type="hidden" value="0"/>
  			<input name="shipmentValue" value="0" type="hidden"/>
  			<input name="currency" type="hidden" value="COP"/>
  			<input name="lng" type="hidden" value="es"/>
  			<input name="approvedResponseUrl" type="hidden" value="http://www.creinmus/perfil.com"/>
  			<input name="declinedResponseUrl" type="hidden" value="http://www.creinmus/venta.com"/>
  			<input name="pendingResponseUrl" type="hidden" value="http://www.creinmus/venta.com"/>
  			<input name="displayShippingInformation" type="hidden" value="YES"/>
  			<input name="sourceUrl" id="urlOrigen" value="" type="hidden"/>
  			<input name="buttonType" value="SIMPLE" type="hidden"/>
  			<input name="signature" value="1668040728a310ce69ead4dc1a7b97ed01edaf22ec3d46015d36b27528cfc582" type="hidden"/>
			</form>
	</div>

</div>	

	@endsection
	</body>
	</html>
	