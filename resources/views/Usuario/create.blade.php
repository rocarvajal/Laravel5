@extends('layout.admin')
@section ('contenido')
<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<h3>Nuevo usuario</h3>
		@if (count($errors)>0)
		<div class="alert alert-danger">
			<ul>
			@foreach ($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
			</ul>
		</div>
		@endif
	</div>
</div>	


		{!!Form::open(array('url'=>'usuario','method'=>'POST','autocomplete'=>'off','files'=>'true'))!!}
		{{Form::token()}}
<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label for="nombre">Nombre</label>
			<input type="text" name="nombre" required="true" value="{{old('nombre')}}" class="form-control" placeholder="Nombre...">	
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label for="apellidos">Apellidos</label>
			<input type="text" name="apellidos" required="true" value="{{old('nombre')}}" class="form-control" placeholder="Apellidos...">	
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label for="correo">Correo</label>
			<input type="text" name="correo" required="true" value="{{old('nombre')}}" class="form-control" placeholder="Correo...">
		</div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label for="">Perfil</label>
            
            <select class="form-control" name="idperfil">
                @foreach($perfil as $usu)
                <option value="{{$usu->idPerfil}}" >{{$usu->nombrePerfil}}</option>
                @endforeach
            </select>	
			
		</div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div>
			<label for="correo">Imagen Perfil</label>
			<input type="file" class="form-control" name="imagen" >	
		</div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<button class="btn btn-primary" type="submit">Guardar</button>
			<button class="btn btn-danger"  type="" >Cancelar</button>
		</div>

	</div>

</div>		
		
				
		{!!Form::close()!!}

		<!--<form class="form-horizontal" id="registro" action="{{ URL::asset('usuario') }}" method="post" >           
            <div class="form-group">
                <label for="inputDato" class="control-label col-xs-2">Dato:</label>
                <div class="col-xs-10">
                    <input type="text" name="dato" id="dato" class="form-control" >                 
                </div>
            </div>       
            <div class="form-group">
                <div class="col-xs-offset-2 col-xs-10">
                    <button type="submit" id="registrar" class="btn btn-primary">Enviar</button>
                </div>
            </div>
        </form>-->

<a href="{{URL::action('UsuarioController@index')}}"><button class="btn btn-info">volver</button></a>

	

	@endsection