@extends('layout.admin')
@section ('contenido')
<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<h3>Nuevo Perfil usuario</h3>
		@if (count($errors)>0)
		<div class="alert alert-danger">
			<ul>
			@foreach ($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
			</ul>
		</div>
		@endif

		{!!Form::open(array('url'=>'perfil','method'=>'POST','autocomplete'=>'off'))!!}
		{{Form::token()}}
		<div class="form-group">
			<label for="nombre">Nombre Perfil</label>
			<input type="text" name="nombrePerfil" class="form-control" placeholder="Nombre Perfil">	
		</div>
		<div class="form-group">
			<label for="Descripcion Perfil">Descripcion Perfil</label>
			<TEXTAREA rows="4" cols="50" type="text" name="descripcionPerfil" class="form-control" placeholder="Descripcion Perfil"></TEXTAREA>	
		</div>	
		<div class="form-group">
			<button class="btn btn-primary" type="submit">Guardar</button>
			<button class="btn btn-danger"  type="" >Cancelar</button>
		</div>
		
		{!!Form::close()!!}

	</div>

</div>	

	@endsection