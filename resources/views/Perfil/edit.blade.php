@extends('layout.admin')
@section ('contenido')
<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<h3>Editar Perfil Usuario : {{ $perfil->nombrePerfil }}</h3>
		@if (count($errors)>0)
		<div class="alert alert-danger">
			<ul>
			@foreach ($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
			</ul>
		</div>
		@endif

		{!!Form::model($perfil,['method'=>'PATCH','route'=>['perfil.update',$perfil->idPerfil]])!!}
		{{Form::token()}}
		<div class="form-group">
			<label for="nombre Perfil">Nombre Perfil</label>
			<input type="text" name="nombrePerfil" class="form-control" value="{{$perfil->nombrePerfil}}" placeholder="Nombre Perfil...">	
		</div>
		<div class="form-group">
			<label for="descripcion Perfil">Descripcion Perfil</label>
			<input type="text" name="descripcionPerfil" class="form-control" value="{{$perfil->descripcionPerfil}}" placeholder="Descripcion...">	
		</div>
		<div class="form-group">
			<button class="btn btn-primary" type="submit">Guardar</button>
			<button class="btn btn-danger" type="reset">Cancelar</button>
		</div>
		{!!Form::close()!!}

	</div>

</div>	

	@endsection