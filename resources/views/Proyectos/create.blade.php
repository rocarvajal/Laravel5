@extends('layout.admin')
@section ('contenido')
<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<h3>Nuevo Proyecto</h3>
		@if (count($errors)>0)
		<div class="alert alert-danger">
			<ul>
			@foreach ($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
			</ul>
		</div>
		@endif
	</div>
</div>	


		{!!Form::open(array('url'=>'proyecto','method'=>'POST','autocomplete'=>'off','files'=>'true'))!!}
		{{Form::token()}}
<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label for="nombre">Nombre Proyecto</label>
			<input type="text" name="nombreProyecto" required="true" value="{{old('nombre')}}" class="form-control" placeholder="Nombre...">	
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label for="">Perfil</label>
            
            <select class="form-control" name="idUsuario">
                @foreach($proyecto as $usu)
                <option value="{{$usu->id}}" >{{$usu->nombre}}</option>
                @endforeach
            </select>	
			
		</div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<button class="btn btn-primary" type="submit">Guardar</button>
			<button class="btn btn-danger"  type="" >Cancelar</button>
		</div>

	</div>

</div>		
		
				
		{!!Form::close()!!}

<a href="{{URL::action('ProyectoUsuarioController@index')}}"><button class="btn btn-info">volver</button></a>

	

	@endsection