@extends('layout.admin')
@section ('contenido')
<div class="row">
<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">

	<h3>Listado de proyectos <a href="proyecto/create"><button class="btn btn-success">Nuevo</button></a></h3><br>
	@include('Proyectos.search')
</div>
</div>

<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="table-responsive">
	<table class="table table-striped table-bordered table-condensed table-hover">
		<thead>
			<th>Id Proyecto</th>
			<th>Id Usuario</th>
			<th>Nombre  </th>
			<th>Correo</th>
			<th>Nombre Proyecto</th>
			<th>Fecha Creacion</th>
			<th>Opciones  </th>
		</thead>

		@foreach ($usuario as $pro)
		

		<tr>
			<td>{{ $pro->idProyecto}}</td>
			<td>{{ $pro->id}}</td>
			<td>{{ $pro->nombre}}</td>
			<td>{{ $pro->correo}}</td>
			<td>{{ $pro->nombreProyecto}}</td>
			<td>{{ $pro->fechaCreacion}}</td>


			<td>
				<a href="{{URL::action('ProyectoUsuarioController@edit',$pro->idProyecto)}}"><button class="btn btn-info">Editar</button></a>
				<a href="" data-target="#modal-delete-{{$pro->idProyecto}}" data-toggle="modal"><button class="btn btn-danger">Eliminar</button></a>
			</td>
		</tr>
		
		@include('Proyectos.modal')
		@endforeach
	</table>
	</div>
	{{$usuario->render()}}
</div>	
</div>
	
@endsection