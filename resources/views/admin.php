<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Admin</title>
	<!--Fuentes de google-->
	<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,700" rel="stylesheet">

	<!-- Bootstrap-->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	
	<!--Estilo css-->
	<link rel="stylesheet" href="../css/estilos.css" class="stylesheet">
</head>
<body class="admin" ">

<!--aca empieza el contendeor del formulario-->
<div class="container">
	<div class="row">
		<div class="col-md-offset-5 col-md-3 col-xs-12 formulario">
		<!--definicion del formulario-->
			<form action="#" method="POST" name="formulario"  class="form-online">
                <div class="form-login">
                <br>
                	<h4 align="center">Bienvenido</h4>
                	<br>
               		 <input required type="text" name="correo" class="form-control input-sm chat-input" placeholder="usuario" />
               		 </br>
                	<input  required type="password" name="password" class="form-control input-sm chat-input" placeholder="contraseña" />
               		 </br>
                	<div class="wrapper">
                		<span class="group-btn">  
               	   	 		<button  type="submit" name="enviar" class="btn btn-prymary btn-md" >Enviar <i class="fa fa-sign-in"></i></button><br><br>


                   <!-- <a href="verificar_usuario.php" class="btn btn-primary btn-md">Entrar <i class="fa fa-sign-in"></i></a>-->
               			 </span>
                  	 
                	</div>
                </div>
               
            </form><!--terminacion del formulario-->
		</div>



	</div>
	



</div>
<!--cierre del contenerdor del formulario-->
</body>
</html>