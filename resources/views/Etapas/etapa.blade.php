@extends('layout.admin')
@section ('contenido')
<div class="row">
<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">

	<h3>Listado de Etapas <a href="etapa/create"><button class="btn btn-success">Nuevo</button></a></h3><br>
	@include('Etapas.search')
</div>
</div>

<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="table-responsive">
	<table class="table table-striped table-bordered table-condensed table-hover">
		<thead>
			
			<th>Nombre Etapa</th>
			<th>Nombre Etapa</th>
			<th>Imagen Etapa</th>
			<th>Link Video Tutorial</th>
			
			<th>Opciones  </th>
		</thead>

		@foreach ($etapa as $usu)

		

		<tr>
			<td>{{ $usu->idEtapa}}</td>
			<td>{{ $usu->nombreEtapa}}</td>
			<td>
				 <img src="{{asset('imagenes/usuario/'.$usu->imagen)}}" alt="{{ $usu->nombreEtapa}}" height="100px" width="100px" class="image-thumbnail">
			</td>
			<td>{{ $usu->linkVideoTutorial}}</td>
			<td>
				<a href="{{URL::action('EtapasUsuarioController@edit',$usu->idEtapa)}}"><button class="btn btn-info">Editar</button></a>
				<a href="" data-target="#modal-delete-{{$usu->idEtapa}}" data-toggle="modal"><button class="btn btn-danger">Eliminar</button></a>
			</td>
		</tr>
		@include('Etapas.modal')
		@endforeach
	</table>
	</div>
	{{$etapa->render()}}
</div>	
</div>
	
@endsection