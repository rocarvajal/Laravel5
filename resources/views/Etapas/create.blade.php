@extends('layout.admin')
@section ('contenido')
<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<h3>Nueva Etapa</h3>
		@if (count($errors)>0)
		<div class="alert alert-danger">
			<ul>
			@foreach ($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
			</ul>
		</div>
		@endif

		{!!Form::open(array('url'=>'etapa','method'=>'POST','autocomplete'=>'off','files'=>'true'))!!}
		
		{{Form::token()}}
		<div class="form-group">
			<label for="nombre">Nombre Etapa</label>
			<input type="text" name="nombreEtapa" class="form-control" placeholder="Nombre...">	
		</div>
		<div class="form-group">
			<label for="descripcion">Descripcion</label>
			<input type="text" name="descripcion" class="form-control" placeholder="descripcion...">	
		</div>
		
		<div>
			<label for="imagen">Imagen Etapa</label>
			<input type="file" class="form-control" name="imagen" >	
		</div>
		
		<div class="form-group">
			<label for="correo">Link Video Toturial</label>
			<input type="text" name="linkVideoTutorial" class="form-control" placeholder="link Video">	
		</div>
		<div class="form-group">
			<label for="">Proyecto</label>
            
            <select class="form-control" name="idProyecto">
                @foreach($etapa as $usu)
                <option value="{{$usu->idProyecto}}" >{{$usu->nombreProyecto}}</option>
                @endforeach
            </select>	
			
		</div>	
		<div class="form-group">
			<button class="btn btn-primary" type="submit">Guardar</button>
			<button class="btn btn-danger"  type="" >Cancelar</button>
		</div>
		
		{!!Form::close()!!}

	</div>

</div>	

	@endsection