<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//manejo de ;a tabla de la base de datos
class PerfilUsuario extends Model
{
    protected $table='perfilusuario';

    protected $primaryKey='idPerfil';

    public $timestamps=false;

    protected $fillable = [
        'nombrePerfil', 'descripcionPerfil',
    ];

     /*public function perfilUsuario()
    {
         return $this->hasOne('App\Usuario');
    }*/
    
}