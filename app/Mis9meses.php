<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//manejo de ;a tabla de la base de datos
class Mis9meses extends Model
{
    protected $table='misprimeros9mese';

    protected $primaryKey='id';

    public $timestamps=false;

    protected $fillable = [
        'nombre', 'correo',
    ];

     
}