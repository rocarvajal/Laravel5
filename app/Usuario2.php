<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//manejo de ;a tabla de la base de datos
class Usuario2 extends Model
{
    protected $table='expectativa';

    protected $primaryKey='id';

    public $timestamps=false;

    protected $fillable = [
        'nombre', 'correo',
    ];
    
}
