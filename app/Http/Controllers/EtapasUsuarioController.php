<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\EtapasUsuario;
use App\ProyectosUsuario;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\EtapasUsuarioFormRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;




class EtapasUsuarioController extends Controller
{
    
    public function _construct (){

    }
    public function index (Request $request){

    	if ($request)
    	{
    		$query=trim($request->get('searchText'));
    		$etapa=DB::table('etapa')
                            //->where ('condicion','=','1')
            //->join('proyecto','etapa.idProyecto','=','proyecto.idProyecto')
            //->select('etapa.idEtapa','etapa.nombreEtapa','etapa.imagenEtapa','proyecto.nombreProyecto')
            ->select('etapa.idEtapa','etapa.nombreEtapa','imagen','linkVideoTutorial')
                ->where('etapa.nombreEtapa','LIKE','%'.$query.'%')
                //->orwhere('proyecto.nombreProyecto','LIKE','%'.$query.'%')
                ->paginate(7);
    		return view('Etapas.etapa',["etapa"=>$etapa,"searchText"=>$query]);
            
             
    	}
    }
    public function create (Request $request){
        $etapa = DB::table('proyecto')->get();
        return view('Etapas.create',["etapa"=>$etapa]);
    }
	public function store (EtapasUsuarioFormRequest $request){

		$etapa=new EtapasUsuario;
		$etapa->nombreEtapa=$request->get('nombreEtapa');
		$etapa->descripcion=$request->get('descripcion');
		//$usuario->condicion='1';

         if(Input::hasfile('imagen')){
            $file=Input::file('imagen');
            $file->move(public_path().'/imagenes/usuario/',$file->getClientOriginalName());
            $etapa->imagen=$file->getClientOriginalName();
        }

        $etapa->linkVideoTutorial=$request->get('linkVideoTutorial');
        $etapa->idProyecto=$request->get('idProyecto');

		$etapa->save();
		return Redirect::to('etapa');
    }
    public function show ($idEtapa){

    	return view("EtapasUsuario.show",["etapas"=>EtapasUsuario::findOrFail($idEtapa)]);
    }
    public function edit ($id){
        $usuario=Usuario::findOrFail($id);
        $perfilusuario=DB::table('perfilusuario')->get();
    	return view("Usuario.edit",["usuario"=>Usuario::findOrFail($id)]);
    }
    public function update (UsuarioFormRequest $request,$id){

    	$usuario=Usuario::findOrFail($id);
    	/*$usuario->nombre=$request->get('nombre');
    	$usuario->apellidos=$request->get('apellidos');
        $usuario->correo=$request->get('correo');*/
        $usuario->update($request->all());
    	//$usuario->update;



    	return Redirect::to('usuario');
    }
    public function destroy ($idEtapa){

    	/*$usuario=Usuario::findOrFail($id);
    	$usuario->condicion='0';
    	$usuario->update();
        Session::flash('message','El usuario fue eliminado'):*/
        EtapasUsuario::destroy($idEtapa);
        return Redirect::to('etapa');
    }
}
