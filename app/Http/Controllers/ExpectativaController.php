<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Usuario2;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\ExpectativaFormRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;



class ExpectativaController extends Controller
{
    
    public function _construct (){

    }
    public function index (Request $request){

    	/*if ($request)
    	{
    		$query=trim($request->get('searchText'));
    		$usuario2=DB::table('expectativa')->where('nombre','LIKE','%'.$query.'%')
                //->where ('condicion','=','1')
                ->paginate(7);
    		return view('expectativa',["usuario2"=>$usuario2,"searchText"=>$query]);
            
             
    	}*/
    }
    public function create (Request $request){

        //$usuarios2 = Usuario2::all();
        return view('expectativa');
    }
	public function store (ExpectativaFormRequest $request){

		$usuario2=new Usuario2;
		$usuario2->nombre=$request->get('nombre');
        $usuario2->correo=$request->get('correo');
        $usuario2->id=$request->get('id');
		//$usuario->condicion='1';
		$usuario2->save();
		return Redirect::to('expectativa2');
    }
    public function show ($id){

    	return view("expectativa.show",["expectativa"=>Usuario2::findOrFail($id)]);
    }
   
   
}
