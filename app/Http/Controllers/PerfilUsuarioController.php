<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Usuario;
use App\PerfilUsuario;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\PerfilUsuarioFormRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;



class perfilUsuarioController extends Controller
{
    
    public function _construct (){

    }
    public function index (Request $request){

    	if ($request)
    	{
    		$query=trim($request->get('searchText'));
    		$perfil=DB::table('perfilUsuario')->where('nombrePerfil','LIKE','%'.$query.'%')
                //->where ('condicion','=','1')
                ->paginate(7);
    		return view('Perfil.perfil',["perfil"=>$perfil,"searchText"=>$query]);
            
             
    	}
    }
    public function create (Request $request){

        $perfil = PerfilUsuario::all();
        return view('perfil.create', compact('perfil'));
    }
	public function store (PerfilUsuarioFormRequest $request){

		$perfil=new PerfilUsuario;
		$perfil->nombrePerfil=$request->get('nombrePerfil');
		$perfil->descripcionPerfil=$request->get('descripcionPerfil');
		//$usuario->condicion='1';
		$perfil->save();
		return Redirect::to('perfil');
    }
    public function show ($idPerfil){

    	return view("perfil.show",["perfil"=>PerfilUsuario::findOrFail($idPerfil)]);
    }
    public function edit ($idPerfil){

    	return view("Perfil.edit",["perfil"=>PerfilUsuario::findOrFail($idPerfil)]);
    }
    public function update (PerfilUsuarioFormRequest $request,$idPerfil){

    	$perfil=PerfilUsuario::findOrFail($idPerfil);
    	/*$usuario->nombre=$request->get('nombre');
    	$usuario->apellidos=$request->get('apellidos');
        $usuario->correo=$request->get('correo');*/
        $perfil->update($request->all());
    	//$usuario->update;



    	return Redirect::to('perfil');
    }
    public function destroy ($idPerfil){

    	/*$usuario=Usuario::findOrFail($id);
    	$usuario->condicion='0';
    	$usuario->update();
        Session::flash('message','El usuario fue eliminado'):*/
        PerfilUsuario::destroy($idPerfil);
        return Redirect::to('perfil');
    }
}