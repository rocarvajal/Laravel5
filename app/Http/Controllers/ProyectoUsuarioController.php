<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use App\ProyectosUsuario;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProyectoUsuarioFormRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;

class ProyectoUsuarioController extends Controller
{
    //
 public function _construct (){

    }
    public function index (Request $request){

    	if ($request)
    	{
    		$query=trim($request->get('searchText'));
    		$usuario=DB::table('proyecto')
                            //->where ('condicion','=','1')
            ->join('usuario2','usuario2.id','=','proyecto.idUsuario')
            ->select('usuario2.id','usuario2.nombre','usuario2.correo','proyecto.nombreProyecto','proyecto.fechaCreacion','idProyecto')
            //->select('proyecto.idProyecto','proyecto.nombreProyecto','proyecto.fechaCreacion')
                ->where('proyecto.nombreProyecto','LIKE','%'.$query.'%')
                //->orwhere('usuario2.id','LIKE','%'.$query.'%')
                ->paginate(7);
    		return view('Proyectos.proyecto',["usuario"=>$usuario,"searchText"=>$query]);
            
             
    	}
    }
    public function create (Request $request){

        $proyecto = DB::table('usuario2')->get();
        return view('Proyectos.create',["proyecto"=>$proyecto]);
    }
	public function store (ProyectoUsuarioFormRequest $request){

		$proyecto=new ProyectosUsuario;
		$proyecto->nombreProyecto=$request->get('nombreProyecto');
		$proyecto->idUsuario=$request->get('idUsuario');
		//$usuario->condicion='1';

		$proyecto->save();
		return Redirect::to('proyecto');
    }
    public function show ($idProyecto){

    	return view("Proyectos.show",["proyecto"=>ProyectosUsuario::findOrFail($idProyecto)]);
    }
    public function edit ($idProyecto){
        $proyecto=ProyectosUsuario::findOrFail($idProyecto);
        $usuario=DB::table('usuario2')->get();
    	return view("Proyectos.edit",["proyecto"=>ProyectosUsuario::findOrFail($idProyecto)]);
    }
    public function update (ProyectoUsuarioFormRequest $request,$idProyecto){

    	$proyecto=ProyectosUsuario::findOrFail($idProyecto);
    	/*$usuario->nombre=$request->get('nombre');
    	$usuario->apellidos=$request->get('apellidos');
        $usuario->correo=$request->get('correo');*/
        $proyecto->update($request->all());
    	//$usuario->update;



    	return Redirect::to('proyecto');
    }
    public function destroy ($idProyecto){

    	/*$usuario=Usuario::findOrFail($id);
    	$usuario->condicion='0';
    	$usuario->update();
        Session::flash('message','El usuario fue eliminado'):*/
        ProyectosUsuario::destroy($idProyecto);
        return Redirect::to('proyecto');
    }
}
