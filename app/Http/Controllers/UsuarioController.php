<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Usuario;
use App\PerfilUsuario;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\UsuarioFormRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;




class UsuarioController extends Controller
{
    
    public function _construct (){

    }
    public function index (Request $request){

    	if ($request)
    	{
    		$query=trim($request->get('searchText'));
    		$usuario=DB::table('usuario2')
                            //->where ('condicion','=','1')
            ->join('perfilusuario','usuario2.idperfil','=','perfilusuario.idPerfil')
            ->select('usuario2.id','usuario2.nombre','usuario2.apellidos','usuario2.correo','perfilusuario.nombrePerfil','perfilusuario.descripcionPerfil','usuario2.imagen')
                ->where('usuario2.nombre','LIKE','%'.$query.'%')
                ->orwhere('perfilusuario.nombrePerfil','LIKE','%'.$query.'%')
                ->paginate(7);
    		return view('usuario.usuario',["usuario"=>$usuario,"searchText"=>$query]);
            
             
    	}
    }
    public function create (Request $request){

        $perfil = DB::table('perfilusuario')->get();
        return view('usuario.create',["perfil"=>$perfil]);
    }
	public function store (UsuarioFormRequest $request){

		$usuario=new Usuario;
		$usuario->nombre=$request->get('nombre');
		$usuario->apellidos=$request->get('apellidos');
        $usuario->correo=$request->get('correo');
        $usuario->idperfil=$request->get('idperfil');
		//$usuario->condicion='1';

        if(Input::hasfile('imagen')){
            $file=Input::file('imagen');
            $file->move(public_path().'/imagenes/usuario/',$file->getClientOriginalName());
            $usuario->imagen=$file->getClientOriginalName();
        }

		$usuario->save();
		return Redirect::to('usuario');
    }
    public function show ($id){

    	return view("Usuario.show",["usuario"=>Usuario::findOrFail($id)]);
    }
    public function edit ($id){
        $usuario=Usuario::findOrFail($id);
        $perfilusuario=DB::table('perfilusuario')->get();
    	return view("Usuario.edit",["usuario"=>Usuario::findOrFail($id)]);
    }
    public function update (UsuarioFormRequest $request,$id){

    	$usuario=Usuario::findOrFail($id);
    	/*$usuario->nombre=$request->get('nombre');
    	$usuario->apellidos=$request->get('apellidos');
        $usuario->correo=$request->get('correo');*/
        $usuario->update($request->all());
    	//$usuario->update;



    	return Redirect::to('usuario');
    }
    public function destroy ($id){

    	/*$usuario=Usuario::findOrFail($id);
    	$usuario->condicion='0';
    	$usuario->update();
        Session::flash('message','El usuario fue eliminado'):*/
        Usuario::destroy($id);
        return Redirect::to('usuario');
    }
}
