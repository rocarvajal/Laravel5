<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Mis9mesesFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'=>'required|max:50',
            'correo'=>'email|max:100|unique:misprimeros9mese'
        ];
    }
}
