<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProyectosUsuario extends Model
{
    Protected $table='proyecto';

    protected $primaryKey='idProyecto';

    public $timestamps=false;

    protected $fillable = [
        'nombreProyecto', 'idUsuario','fechaCreacion'
    ];
}
