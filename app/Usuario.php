<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//manejo de ;a tabla de la base de datos
class Usuario extends Model
{
    protected $table='usuario2';

    protected $primaryKey='id';

    public $timestamps=false;

    protected $fillable = [
        'nombre', 'apellidos', 'correo', 'idperfil','imagen',
    ];

     /*public function perfilUsuario()
    {
         return $this->hasOne('App\Usuario');
    }*/
    
}
