<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//manejo de ;a tabla de la base de datos
class EtapasUsuario extends Model
{
    protected $table='etapa';

    protected $primaryKey='idEtapa';

    public $timestamps=false;

    protected $fillable = [
        'nombreEtapa', 'descripcion', 'imagenEtapa', 'linkVideoTutorial'
    ];

     /*public function perfilUsuario()
    {
         return $this->hasOne('App\Usuario');
    }*/
    
}
