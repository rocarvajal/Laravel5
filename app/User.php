<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table='usuario2';

    protected $primaryKey='id';
    protected $fillable = [
        'nombre', 'correo', 'idperfil',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
         'remember_token',
    ];

    /*public function perfil(){

        return $this->hasMany(perfilUsuario::class);
    }*/
}

